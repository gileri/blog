AUTHOR = "gileri"
SITENAME = "Blog"
SITEURL = "https://blog.linuxw.info"

PATH = "content"

TIMEZONE = "Europe/Paris"

DEFAULT_LANG = "fr"

THEME = "themes/pelican-alchemy/alchemy"
PYGMENT_STYLE = "fruity"

LINKS = (("Home", "https://eric.linuxw.info/"),)

FOOTER_LINKS = (
    ("License : CC-BY-SA 4.0", "https://creativecommons.org/licenses/by-sa/4.0/"),
    ("Sources", "https://gitlab.com/gileri/blog"),
)

DEFAULT_PAGINATION = False

MARKDOWN = {
    "extension_configs": {
        "markdown.extensions.codehilite": {
            "css_class": "highlight",
            "linenums": True,
            "guess_lang": False,
        },
        "markdown.extensions.extra": {},
        "markdown.extensions.meta": {},
    },
    "output_format": "html5",
}

RELATIVE_URLS = True
