Title: Respiration et taux de CO2 pendant le sommeil
Date: 2021-08-07
Category: Santé

TL;DR Pendant le sommeil le CO2 expiré peut poser problème, mais si la ventilation (VMC) fonctionne l'air est largement assez renouvellé.

Des fois je dors mal, et j'avais l'impression que c'était corrélé avec les jours d'été où j'ouvrais les fenêtres de mon salon la nuit, mais pas la chambre.
En faisant ça, l'air que j'expire la nuit n'est pas remplacé car la VMC aspire l'air depuis les autres pièces uniquement, et ça causerait des problèmes peut-être. J'ai voulu vérifier :

[L'air expiré a 4-5% de volume en CO2, 100 fois plus que l'air inspiré](https://en.wikipedia.org/wiki/Breathing#Composition).
Au repos, on [inspire ou expire environ 5 litres par minute d'air](https://en.wikipedia.org/wiki/Minute_ventilation). Il n'y a pas de sources dans l'article, mais en comparant très scientifiquement avec ma respiration ça a l'air raisonnable.
Au bout de 8h, un adulte aura ajouté 120 litres de CO2.

Une chambre classique fait 25m3 (12m2 multiplié par une 2.5m de hauteur de plafond, moins 20% de meubles).
L'air de base contient en volume 0.05% de CO2*, soit 15L dans cette chambre. À cela il faut ajouter les 120L expirés, qu'il faut multiplier par le nombre de personnes.

La [densité moyenne de l'air 1.2kg/m3](https://en.wikipedia.org/wiki/Density_of_air, celle du [CO2 est 53% plus élevée](https://en.wikipedia.org/wiki/Carbon_dioxide) donc 1.8kg/m3.
Il y a donc 36kg d'air, dont 250g de CO2. Soit une concentration de 0.7%.

Le [seuil d'impact physiologique notable semble être 0.5%](https://en.wikipedia.org/wiki/Carbon_dioxide#Toxicity).
La ventilation de la chambre apparaît indispensable. Mais heureusement elle est [obligatoire, avec un minimum de 5m3 par heure environ pour cette pièce](https://www.legifrance.gouv.fr/loda/id/LEGITEXT000006074206/), compensant largement l'air expiré par 2 personnes (0.6m3).

\* en ville le taux de CO2 est vraisemblablement plus haut
