Title: Achat et livraison de logement collectif en VEFA (neuf sur plan)
Date: 2021-08-07
Category: Logement

En 2019 j'ai fait le grand saut et acheté mon premier appartement. Enfin plutôt acheté un appartement qui sera construit un peu plus de deux ans après.
C'était une expérience longue, demandant beaucoup de temps, et qui n'est pas finie même plusieurs mois après la livraison.
Je me suis dit que mon expérience pourrait aider de futurs acquéreurs.

# Le crédit

Avant même de commencer le processus d'achat, il faut s'assurer que l'on peut avoir les fonds, généralement par crédit.
Les banques fournissaient des taux très intéressant, mais passer par un courtier permet généralement d'avoir un taux encore plus bas (ce qui est étonnant vu qu'un courtier prends une commission...).
Une erreur à ne pas faire est de faire un apport trop élevé. Ça permet d'obtenir des taux un peu plus bas, et une durée de crédit plus faible, mais c'est pas forcément rentable par rapport à "placer" son argent ailleurs. Le [livret A va bientôt passer à un taux d'intérêt de 1%](https://www.service-public.fr/particuliers/vosdroits/F2365), si votre TAEG est inférieur à 1% vaut mieux le mettre dans le livret. Et plein d'autres placements ont des taux d'intérêt attendus supérieurs à 1%.

# L'achat
Facile à dire, moins à faire, mais attention à ne pas paraître trop "nouveau" dans le game. Ça réduit les possibilités de négociation, et augmente les chances de se faire pigeonner. Il peut y avoir des mensonges, tant que c'est à l'oral et pas dans le descriptif notaire c'est comme si ça n'existait pas.
J'ai découvert par la suite que mes voisins du dessus faisaient installer une piscine sur mon plafond, et ça ils se sont gardés de le dire.
Idem, la vendeuse a fait miroiter que les parking sous-terrains seraient boxés, mais en insistant ça s'est transformé en "non-boxé", puis "non-boxable". Il est crucial de faire mettre toute zone de doute à l'écrit, même si ça fait passer pour un emmerdeur.

# L'avancée des travaux et les appels de fonds

Le promoteur peut demander les fonds après l'achèvement de chaque étape, certifiée par l'architecte. Il ne peut pas demander les fonds avant.

En effet, en cas d'écart de conformité important, mais ne rendant pas impropre à l'usage le logement, [l'acheteur est dans son plein droit de recevoir les clés, tout en consignant les 5% du prix restant](https://www.service-public.fr/particuliers/vosdroits/F2956).
Cela peut être par exemple :
* Pièces qui n'ont pas la bonne taille
* Hauteur sous plafond incorrecte (attention les faux-plafonds peuvent changer la hauteur, tout en restant conforme dans certains cas)

Cependant certains promoteurs, dont celui que j'ai connu, conditionnent la remise des clés au paiement de la dernière tranche. Ce chantage illégal leur permet de jouer sur les frais engendrés par l'impossibilité d'emménager si l'acheteur conteste la conformité.
Difficile d'éviter ce problème, mais ils peuvent accepter un chèque de banque le jour de la livraison. Cela permet de plus facilement refuser le logement en cas de problème conséquent.

# Livraison des parties communes

La livraison des parties communes intervient généralement un peu avant la livraison des logements, et est réalisée par le syndic, accompagné du conseil syndical.
Il existe un délai de 30 jours pour signaler tout défaut "superficiel", qui sera réputé comme existant au moment de la livraison.
Mais selon le niveau d'honnêteté du promoteur, il pourra essayer de dire que des défauts proviennent d'une dégradation par les propriétaires. C'est pour cela qu'il vaut bien mieux passer plus de temps le jour même pour être exhaustif, dans ce cas il sera plus compliqué de nier l'existance des défauts lors de la livraison.

Autre problème : la correction de défauts de livraison (ou dans le cadre des garanties), engendre généralement d'autres problèmes : marques sur les murs, tâches de peinture, portes abîmées, etc.
Tant que possible il faut que le conseil syndical, ainsi que les occupants, surveillent les ouvriers afin de pouvoir imputer les dégâts à ces derniers.

# Livraison des parties privatives

Les règles sont similaires aux parties communes, mais les moyens de pressions sont plus faibles. C'est pour cela qu'il faut conserver la pression sur le promoteur, tout en oubliant pas que l'on a des humains en face ;)
Rien ne sert d'insulter oude prendre de haut les interlocuteurs, ça va les bloquer. Mais il faut se montrer ferme et compétent pour pas se faire marcher sur les pieds.

L'appartement a été livré sale. J'ai voulu être arrangeant, et me suis dit que je pouvais tout à fait faire le nettoyage facilement moi-même. Grosse erreur.
* Les fenêtres n'étaient pas sales, elles avaient de la lasure transparente incrustée, ainsi que des micro-impacts.
* Le platre dans les joints entre les carreaux est très difficile à enlever
* La ferronerie était bien rayée, sous le platre pas nettoyé

En résumé, exiger un nettoyage c'est pas être flemmard, mais juste pouvoir s'assurer que les revêtements sont en bon état. Les promoteurs profitent bien de cette sympathie pour livrer des biens endommagés.

Autre point d'attention sur les ouvriers un peu moins qualifés (peinture par exemple). Ils n'ont pas vraiment d'intérêt à faire un travail exhaustif, ou de qualité, ils sont payés à l'heure. Il faut surveiller le travail, même si c'est désagréable pour l'acheteur et l'ouvrier, mais les intérêts des deux sont opposés...

# Conclusion

En résumé ça fait quand même bien plaisir de recevoir le logement, et le fait d'être propriétaire permet d'avoir une légère "sécurité" par rapport à une location.
Mais ce n'est pas une mince affaire, qui demande des recherches, des travaux, et au total des dizaines d'heures de travail pour obtenir un bien conforme.
