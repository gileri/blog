Title: Autopartage Tesla Model 3 : Auto-partage et financier
Date: 2023-09-03
Category: Mobilité

Suite de [ce précédent article]({filename}/retour-model-3_1.md)

# Auto-partage

C'est la raison principale pour laquelle je fais cet article. C'est une pratique qui prend de l'ampleur, mais qui pourrait être plus répandue.

Cette voiture est très bien adaptée au partage :

* On peut créer des profils pour que les locataires aient leurs préférences
* Le partage de conduite est facile via l'application mobile, et la clé au format carte bancaire (qui coûte pas cher à remplacer par ailleurs).
* Vu le prix et le gabarit, elle est faite pour les longs voyages. La recharge rapide permet de les faire facilement, contrairement à d'autres véhicules, notamment français, qui ont la recharge rapide qu'en option, et qui reste plutôt lente.
* Les assistances à la conduite permettent de sécuriser un peu la conduite, pour les débutants ou en cas d'inattention.

Sur ces 18 mois j'ai pu la louer 50 jours pour un total de 14000 km. Le facteur limitant étant surtout la demande. Le coût payé par le locataire est relativement élevé (moindre qu'un pro tout de même), et les gens n'ont pas encore le réflexe autopartage.
Les locataires sont en général polis, ponctuels et honnêtes, mais il faut rester vigilant. Sur ces locations, j'ai eu :

* 1 panne sèche sur autoroute. Les informations de la plateforme étaient contradictoires, mais après avoir passé 2h répartis sur plusieurs appels téléphonique, je n'ai rien eu à payer.
* 1 impact de pare-brise, remboursé à 60€ alors que la réparation m'a couté 140€. Peu de garages acceptent de faire des réparations d'impact, car en cas de casse du pare-brise pendant l'opérations, ils ne savent pas remonter et calibrer les caméras.
* 3 fois les jantes raclées. 2 personnes ont essayé de s'y soustraire, mais j'ai pu finalement obtenir de maigre compensation en espèces. Il faut avoir la tête dure et pas se laisser marcher sur les pieds. À chaque fois je les[repeint, ou répare si nécessaire moi-même](https://www.youtube.com/watch?v=jhePZhjB3PU)
* 2 fois la voiture empestée à la cigarette. Le forfait dans ce cas est bien trop faible, et nécessite de passer 1 à 2h de main d'oeuvre pour enlever l'odeur si on passe pas par un pro. Après avoir enlevé la cendre méticuleusement, il existe des spray pour enlever ces odeurs (par exemple celui de Michelin). J'y croyais pas, mais ça fonctionne bien.
* 1 accident : coffre embouti, pare-choc avant et arrière rayés. Ça a pris 4 mois pour obtenir la réparation. Les échanges entre assurance, expert, garage étaient vraiment épuisants. Mais à la sortie, le véhicule était nickel, même s'il y a maintenant de légères différences de teinte. Le garage agréé de mon coin fait quand même du bon boulot.
* Une personne qui m'a peut-être présenté un faux permis. Je n'ai pas osé refuser, mais rien ne s'est passé par chance. Pas sûr de comment authentifier un vrai permis. Je sais que la plateforme ne vérifie pas bien les permis, notamment sur les dates d'obtention, pourtant qui conditionnent l'assurance.

Il y a malheureusement beaucoup de petits incidents. mais à part le pare-brise, ils ont été pris en charge sans frais. Ça génère quand même du stress et peut prendre du temps.
La vérification au retour du véhicule est pas évident, il y a beaucoup de choses à vérifier. Heureusement les locataires sont en général honnêtes et indiquent les problèmes qu'ils ont eu.

En terme de temps, chaque location me prend environ 3 heures : 1 heure de livraison de voiture, 1 heure pour le retour, et 1 heure pour la recharge, nettoyage, et problèmes éventuels.

La location serait plus facile avec un véhicule déjà bien usé, pour simplifier le nettoyage et l'état des lieux. Il faudrait aussi des prix plus élevés plus élevés, mais je n'ai pas l'impression que les gens soient prêt à payer ça. Peut-être avec une petite taxe carbone (#giletsjaune) ça motiverait :p ?

# Le financier
Le prix d'achat de la voiture fluctuait beaucoup. Je l'ai eu à 43800, avec 6000€ de bonus "écologique", ce qui amène à 37800€.
Ça fait bien plus cher que beaucoup d'autres véhicules, surtout d'occasion. Elles n'étaient pas disponible en 2022 à l'achat d'occasion (toutes étaient vendues plus chères que le neuf, à l'étranger) mais il y a quelques aspects à prendre en compte :
* Le véhicule consomme bien moins d'énergie qu'un véhicule thermique sur le cycle de vie. Et l'électricité sera vraisemblablement toujours moins chère que l'essence.
* Bien moins de pièces d'usure (pas d'embrayage, freins peu sollicités, pas de vidange), mais les pièces sont chères. Par exemple les 4 pneus hiver c'est 900€, car ils doivent supporter le poids et la vitesse élevés.

Avec la faible consommation et prix de l'électricité, cela représente actuellement 3€ les 100km à la maison, 5.5€ en charge rapide. À comparer avec 11€ d'essence pour un gabarit similaire (6L/100km).
Sur la durée de vie de la voiture, on peut espérer 12 000 à 15 000 € de différence lié à l'énergie par rapport à une thermique, et cet écart pourrait se creuser avec l'apparition d'une taxe carbone.

Les locations m'ont "rapporté" 3800€. À cela il faut retirer de nombreux frais :

* Le fisc, avec un impôts d'environ 18%
* La Recharge après location (j'offre 1 "plein" à chaque fois) : 25-35€
* L'usure normale du véhicule, surtout les pneumatiques. Mais aussi l'usure anormale : les rayures, l'utiisation moins respectueuse de la partie motrice, etc..
* L'assurance perso qui continue de tourner pendant la location, même si elle est inutilisée.

Avec des prix de locations à environ 75€ la journée et 9€ pour 100km, cela fait environ un revenu par kilomètre semi-net 0.21€/km. Mais la voiture dans l'ensemble coûte environ 0.20€ (achat, pneus, entretien, assurance). Avec le recul, ce n'est donc pas rentable au prix où je le fait payer, qui me paraissait déjà élevé pourtant !
À noter que les locataires ont des tarifs plus élevés d'environ 20%, comprenant l'assurance et les frais de plateforme.

# Conclusion

En conclusion, financièrement ça reste un véhicule assez cher et fragile comparé aux modèles sobres que j'ai connu par le passé. Le rapport qualité/prix reste meilleur sur les véhicule thermique, mais il faut tout de même considérer l'aspect écologique, la présence de ZFE, et les évolutions futures des prix de l'énergie.

L'autopartage peut être fait en sus d'un travail "classique", mais n'est pas rentable pour ce type de véhicule. Mais cela permet tout de même d'éviter que le véhicule reste à dormir sur une place de parking, alors qu'il pourrait remplacer l'usage d'un autre véhicule et ainsi réduire l'impact écologique de la voiture.
