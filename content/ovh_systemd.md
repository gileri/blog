Title: Systemd 246 sur image Archlinux OVH
Date: 2020-09-24
Category: Linux
Lang: fr
Slug: systemd-246-ovh

J'utilise Archlinux sur un serveur OVH Kimsufi depuis 2013, et Systemd a bien changé depuis.
La version 246, disponible en paquets Archlinux depuis fin juillet 2020, retire le [support de la directive `.include`](https://github.com/systemd/systemd/blob/master/NEWS#L197)
Cette directive était utilisée par le service `netctl` de configuration réseau :

```
% cat /etc/systemd/system/netctl@ovh_net_eth0.service
.include /usr/lib/systemd/system/netctl@.service

[Unit]
Description=A basic static ethernet connection
BindsTo=sys-subsystem-net-devices-eth0.device
After=sys-subsystem-net-devices-eth0.device
```

Si le service n'a pas été migré à la nouvelle syntaxe, le serveur n'aura plus de configuration réseau au prochain démarrage :

```
Sep 24 11:25:39 ovh systemd[1]: netctl@ovh_net_eth0.service: Cannot add dependency job, ignoring: Unit netctl@ovh_net_eth0.service has a bad unit file setting.
Sep 24 11:25:39 ovh systemd[1]: netctl@ovh_net_eth0.service: Service has no ExecStart=, ExecStop=, or SuccessAction=. Refusing.
Sep 24 11:25:39 ovh systemd[1]: /etc/systemd/system/netctl@ovh_net_eth0.service:1: Assignment outside of section. Ignoring.
```

Pour migrer la configuration le plus simple est de désactiver la dépendance à ce dernier, le supprimer, et réactiver le service du profil netctl :

```
systemctl disable netctl@ovh_net_eth0.service
rm /etc/systemd/system/netctl@ovh_net_eth0.service
netctl enable ovh_net_eth0
```
