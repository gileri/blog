Title: Tor à la maison
Date: 2020-07-30
Modified: 2020-10-04
Category: Network

Depuis quelques semaines je fais tourner un nœud intermédiaire Tor chez moi. Il ne sert que de point d'entrée dans le réseau, ou de relai intermédiaire, mais pas de nœud de sortie.

Cela veut dire que mes addresses IP ne peuvent être utilisées comme source d'attaque sur le réseau Internet non-Tor.

Et pourtant, depuis j'ai remarqué que je ne peut pas me connecter à plusieurs sites, notamment :

- auchan.fr / auchandrive.fr
- matmut.fr
- aliexpress.com
- compte.laposte.fr

Ces sites-là, ou plus précisément la liste des "nœuds Tor" dont ils se servent, ne fait pas de distinction entre nœud de sortie ou non. 

En résumé, évitez de faire tourner un nœud Tor, même qui ne fait pas de sortie, chez vous.
