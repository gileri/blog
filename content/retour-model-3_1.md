Title: Autopartage Tesla Model 3 : La voiture, sa recharge et son impact écologique
Date: 2023-09-03
Category: Mobilité

J'ai aquis il y a environ 18 mois une Tesla Model 3 d'occasion. Le but était de la partager avec un ami (ça marchait pas bien), et également faire de l'autopartage via une plateforme en ligne, ici Ouicar.

Avec 50 jours de location, et 30 000 km au compteur, je voulais faire un peu retour d'expérience, surtout sur l'aspect autopartage.

# La voiture

C'est une Tesla Model 3 Propulsion 2022
![Tesla Model 3. Crédit Carlquinn CC-BY-SA](/images/model_3.jpg)

Avant ça j'utilisais une Twingo de 1997, le contraste est sévère !
![Twingo 1. Crédit Vauxford sur Wikimedia Commons](/images/twingo_1.jpg)

Je laisse les médias spécialisés présenter le véhicule dans l'ensemble, et je vais plutôt me concentre sur les points saillants.

Aucun problème à l'achat. Les pièces de carrosserie étaient bien alignées, la peinture était correcte.
Au bout de quelques semaines, les roues font un bruit de craquement lorsqu'on braque. Problème réglé en nettoyant à la brosse métallique la surface de contact entre roue et essieu. Il faut le faire à chaque changement de pneus.

La voiture est trop puissante (280kW), c'est un pousse-au-crime.
Avec des pneus pourtant bons, une bonne répartition des masses grâce à la position basse de la batterie, j'ai été un peu déçu du freinage, même si c'est tout à fait correct.

La peinture rouge (PPMR) est très jolie je trouve, mais est fragile. En 2000 km, plusieurs impacts la marquent déjà. Des [stylos de retouches](https://www.peinturecarrossier.fr/peinture-constructeur/52000512-stylo-de-retouche-tesla-codification-ppmr.html?cid=1287865_1055283_PPMR) sont facile à trouver et utiliser, même si c'est fastidieux.
Le pare-brise a l'air tendre également : chaque gravier fait des impacts sur le pare-brise, il se raye facilement. C'est peut-être pour améliorer la protection des piétons lors d'accident que le verre est comme ça ?

# Autonomie/recharge

L'autonomie et vitesse de recharge me sont largement suffisants. Sur chargeur rapide, on arrive à recharger en pointe à 1000 km par heure de charge, en moyenne 700-800km/h.
Ça correspond à 15 minutes de pause toutes les 2h d'autoroute à 130, encore moins à 110. À un tel point qu'au final rouler à 110 ne rallonge quasiment pas le trajet, car les charges sont + rapides.
Sur le trajet le plus long que j'ai fait : Lyon - Rennes, en été (donc clim), les temps de recharge étaient quasi invisibles.

Aucun problème de disponibilité des bornes rapides. Les bornes lentes sont cependant parfois chères (plus que les rapides !), et souvent en partie en panne. Choisir au hasard sa borne peut faire doubler ou tripler l'addition.
Ayant la chance d'avoir des bornes à proximité de mon domicile, et donc recharge sur la voie publique. Cela évite l'installation d'un chargeur à domicile, ce qui est coûteux et superflu si on roule peu.

# La pollution

La Model 3 est lourde, mais moins qu'un SUV. Son bon coefficient de trainée (aérodynamisme), et équipements électriques efficaces permettent d'obtenir une consommation légèrement inférieure à une Renault Zoé, pourtant plus petite et moins lourde.
À sa production, il est estimé que sont émis entre [30](https://www.tesla.com/ns_videos/2022-tesla-impact-report.pdf) et [60 tonnes d'équivalent CO2](https://climobil.connecting-project.lu/?batteryLifetime=200000&batteryCapacity=55&greenhouseGas=65&electricCarRange=491&carbonElectricityMix=105&greenhouseBattery=30&greenhouseWTT=25&greenhouseTTW=138&batteryPenalty=0.9&annualMileage=20000&ICECurb=840&ECurb=1611&NEDCpenalty=0.39&decarbonization=0.1). Mais sur la durée de vie espérée, elle émettra bien moins qu'une thermique. On peut voir cet écart et comment il est calculé avec ce [super outil](https://climobil.connecting-project.lu/?batteryLifetime=200000&batteryCapacity=55&greenhouseGas=65&electricCarRange=491&carbonElectricityMix=105&greenhouseBattery=30&greenhouseWTT=25&greenhouseTTW=138&batteryPenalty=0.9&annualMileage=20000&ICECurb=840&ECurb=1611&NEDCpenalty=0.39&decarbonization=0.1).
Ça représente tout de même environ 4 tonnes de CO2 par an juste en comptant la fabrication, ce qui explose l'objectif visé en 2050 de 2 tonnes par personnes. L'auto-partage poussé au maximum permet de limiter cet impact.

Pour la consommation électrique, la France est l'un des pays les moins émetteur, c'est une chance et c'est ce qui permet d'avoir un tel écart avec un thermique. N'hésitez pas à comparer les différents pays en terme de production grâce à l'outil ci-dessous, qui montre à quel point le mix énergétique d'autres pays européens, notamment l'Allemagne ou la Pologne est émetteur.

En ville, les véhicule électrique n'émettent pas de particules fines lié à la combustion, et avec le freinage régénératif, quasiment aucunes depuis les freins. Cependant, le poids plus élevé, et les pneus plus grands et larges (pour le poids, mais également la vitesse maximum) génèrent plus de particules liés au roulement.

# Conclusion

C'est un bon véhicule agréable, et je suis bluffé par la bonne prestation de l'électrique. Ça reste tout de même un véhicule cher, lourd et polluant, même si bien moins qu'un véhicule thermique.

[Seconde partie ici]({filename}/retour-model-3_2.md)
