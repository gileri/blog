Title: Migration données ReadyNAS RN102 vers Linux récent
Date: 2020-07-19
Category: Storage

Après avoir monté un serveur NAS maison en remplacement de mon [Netgear RN102]({filename}readynas.md), j'ai voulu mettre les deux disques en RAID1 md de mon précédent NAS dans mon serveur sous Archlinux à jour. Le processus est normalement trivial :

* Éteindre le NAS
* Brancher les disques sur le serveur Linux
* Les grappes RAID [md](https://en.wikipedia.org/wiki/Mdadm) sont découvertes automatiquement et rendues accessible sur /dev/md* et /dev/md/*
* Monter les systèmes de fichier Btrfs présent dans /dev/md\*

Évidemment, si je fait un article c'est que ça s'est pas passé comme ça... dès que j'accédais à certains dossiers, j'obtenais les erreurs suivantes :

```dmesg
[11565.673863] BTRFS critical (device md126): corrupt leaf: root=1 block=1955138043904 slot=5, invalid root flags, have 0x10000 expect mask 0x1000000000001
[11565.673871] BTRFS error (device md126): block=1955138043904 read time tree block corruption detected
[11565.681895] BTRFS critical (device md126): corrupt leaf: root=1 block=1955138043904 slot=5, invalid root flags, have 0x10000 expect mask 0x1000000000001
[11565.681902] BTRFS error (device md126): block=1955138043904 read time tree block corruption detected
```

`btrfsck` ne remonte aucunes erreurs, et après des heures de galère impossible de trouver un correctif. En remettant les disques dans le NAS tout refonctionne parfaitement. Le problème se produit qu'avec des noyaux Linux "récents".

J'ai donc essayé de migrer les données entre le ReadyNAS et mon nouveau NAS via rsync sur SSH mais évidemment c'est trop long.
Deuxième tentative avec tar+netcat pour éviter le chiffrement de SSH, toujours trop lent, CPU saturé pour environ 20MB/s.

Au final j'ai obtenu des résultats corrects en mettant les deux disques dans le NAS sous Arch et en migrant les données via une VM avec qemu. C'était l'occasion de tester qemu en CLI au lieu de via libvirt habituellement.

Pour la VM il faut :

* Une version de noyau proche de celle de ReadyNAS OS (4.19.4)
* Utiliser un live linux (évite une installation)
* Les outils Btrfs
* Supporte virtfs et virtio pour que le transfert soit rapide

L'image d'installation [Archlinux 2018-12-01](https://archive.archlinux.org/iso/2018.12.01/archlinux-2018.12.01-x86_64.iso.torrent) coche toutes ces cases. Elle est facilement trouvable grâce à l'[archive Archlinux](https://wiki.archlinux.org/index.php/Arch_Linux_Archive) 

# Machine hôte
## Vérification de l'état des grappes RAID md
```shell
mdadm --detail /dev/md*
```

## Arrêt de la grappe de données
```shell
mdadm --stop /dev/md126
```

## Remise en service de la grappe de données avec qu'un seul disque.
`--run` est nécesaire pour forcer la mise en service avec qu'un seul disque

`--readonly` est pas obligatoire, mais ça évite les mauvaises manips

```shell
mdadm --assemble --run --readonly /dev/md126 /dev/sdb3
```

## Démarrage de la VM
```shell
qemu-system-x86_64 -cdrom archlinux-2018.12.01-x86_64.iso -boot order=d -enable-kvm -machine q35 -cpu host -m 512M -drive file=/dev/md/md_data,format=raw,if=virtio,readonly -vga std -vnc :0 -virtfs local,id=dest,path=/mnt/data,security_model=none,mount_tag=dest -k fr -smp 4
```

[Démarrer une ISO](https://wiki.archlinux.org/index.php/QEMU#Installing_the_operating_system)

`-cdrom archlinux-2018.12.01-x86_64.iso -boot order=d`

[Options de virtualisation machine](https://wiki.archlinux.org/index.php/QEMU#Running_virtualized_system)

`-enable-kvm -machine q35 -cpu host -m 512M -smp 4`

[Partage de disques](https://wiki.qemu.org/Documentation/9psetup)

`-drive file=/dev/md/md_data,format=raw,if=virtio,readonly`

J'ai pas eu la motivation d'utiliser la VM via port série, j'ai utilisé [VNC](https://wiki.archlinux.org/index.php/QEMU#VNC)

`-vga std -vnc :0`

[Forcer la disposition des touches sur la VM](https://wiki.archlinux.org/index.php/QEMU#Keyboard_seems_broken_or_the_arrow_keys_do_not_work)

`-k fr`

#Dans la VM

##Montage des deux systèmes de fichier :

`-o noatime` permet de grapiller un peu de performance

`-o trans=virtio` force le mode de transport (pas sûr que ce soit nécessaire)

```shell
mkdir src dest
mount -t 9p -o trans=virtio,noatime dest dest
mount -t btrfs -o ro /dev/vda src
```
##Copie des fichiers

`-a` pour copier les attributs des fichiers (propriétaires, permissions etc.)

```shell
rsync -a src/ dest
```
# ou
```shell
cp -a src/* dest
```
