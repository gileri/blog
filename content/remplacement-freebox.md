Title: Remplacement d'une Freebox en zone FTTH très dense par un routeur OpenWRT
Date: 2019-01-08
Category: Network

Free FTTH est enfin devenu disponible dans ma résidence, près de deux ans après la construction.
J'ai sauté sur l'occasion ayant croisé la personne ayant réalisé l'installation de l'arrivée dans le point de mutualisation (cave).

Après quelques déboires (mauvaise arrivée NRO choisie par le technicien et raccordement vertical pas finalisé (étage vers cave), j'ai pu moi-même raccorder la jarretière sur le brin de fibre vertical dédié Orange.

Même si la Freebox mini 4K (dérivé de la Révolution) est un bon routeur/bridge, j'ai pas résisté à la remplacer par mon Turris Omnia.

# Mode bridge

Dans ce mode la Freebox ne fait office que de modem, et ne route plus les paquets entre le réseau IP public et privé.
Son activation se fait via l'interface web. Une fois fait, le premier appareil connecté au côté WAN récupère une adresse IPv4 publique. L'administration de la freebox se fait en accédant à mafreebox.free.fr, qui est résolu en l'adresse IPv4 publique de la Freebox.

# Remplacement Total

Remplacer totalement la Freebox implique de connecter la jarretière fibre sur son propre routeur via une interface SFP (celle fournie par Free par exemple).
À noter que je réside en [Zone Très Dense](https://www.arcep.fr/la-regulation/grands-dossiers-reseaux-fixes/la-fibre/le-cadre-reglementaire-de-la-fibre.html); la configuration peut différer selon si l'on est en ADSL/VDSL, Fibre Zone Moyennement Dense ou Très Dense.

## IPv4

Il suffit de configurer une interface réseau [tagguée VLAN 836](https://fr.wikipedia.org/wiki/R%C3%A9seau_local_virtuel#Lien_marqu%C3%A9) et de demander une adresse via DHCP. Aucune authentification requise, pas de filtrage par adresse MAC, c'est le top.

## IPv6

Il était plus difficile de trouver des informations à ce sujet, peu de personnes ont remplacé leurs Freebox à cause de la nécessité d'utiliser un SFP, et encore moins ont expliqué leur démarche.

Afin de faciliter son déploiement, Free emploie le [6rd/IPv6 Rapid Deployment](https://fr.wikipedia.org/wiki/6rd). C'est à dire que le lien abonné-opérateur est toujours en IPv4, mais un "tunnel" 6rd en IPv4 est fourni pour permettre à l'abonné d'avoir des adresses IPv6 sur son réseau local

Le préfixe IPv6 '2a01:e30::/28', ainsi que l'adresse de la passerelle 6rd '192.88.99.101' semblent être les mêmes pour les abonnés.
La seule partie spécifique à chaque abonnés est l'adresse IPv4, qui, ajouté à droite du préfixe IPv6 permet de donner le préfixe complet '/61', laissant la possibilité d'utiliser 8 réseaux '/64' chez soi.
Lorsque l'adresse IPv4 locale n'est pas défini, OpenWRT utilise celle configurée sur l'interface WAN automatiquement.
Une fois ce tunnel effectué, le routeur peut obtenir une adresse et en fournir à d'autres appareils.

/etc/config/network
```
config interface 'WAN6'
  option _orig_ifname 'eth1.836'
  option proto '6rd'
  option peeraddr '192.88.99.101'
  option ip6prefix '2a01:e30::'
  option ip6prefixlen '28'
```
