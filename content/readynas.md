Title: Netgear Readynas RN102
Date: 2020-07-19
Category: Hardware

J'ai acheté en 2014 un [NAS Netgear Readynas RN102](https://www.inpact-hardware.com/article/1076/plongee-au-cur-readynas-os-6-10-interface-gestion-nas-netgear), bas de gamme mais très fonctionnel pour une misère : 80 €.

À l'époque je l'avait choisi pour son tarif, mais aussi le fait qu'il utilisait Btrfs sur md, Debian 8 (avec noyau plus récent) et laissait un accès root SSH.
Après 6 ans de loyaux services, il se trouve trop limité niveau RAM (512M) et CPU (Marvel Armada 370 à 1.2Ghz) pour l'utiliser en tant que NAS, client Torrent et autres services.
