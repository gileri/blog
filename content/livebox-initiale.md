Title: Migration de Livebox Zen Fibre à Livebox Initiale sur OpenWRT/Turris-OS
Date: 2018-11-06
Category: Network

Orange m'a proposé via un courrier de passer d'un abonnement Livebox Zen Fibre à Livebox Initiale, le tout pour faire baisser ma facture.
Une heure après la procédure une heure, plus de WAN malgré la promesse d'ininterruption.
Il se trouve que la méthode d'authentification réseau change entre les deux forfaits, chose quasi-transparent sur une Livebox, mais sur un routeur sur OpenWRT ne met évidemment pas à jour ses options de requêtes DHCP.

Après capture réseau via Wireshark sur la prise "Fibre" RJ-45 du routeur, la différence était facilement visible sur les options d'authentification

# Avant

/etc/config/network
```
 config interface 'wan'
   option sendopts '0x4D:2b46535644534c5f6c697665626f782e496e7465726e65742e736f66746174686f6d652e4c697665626f7834 0x5a:0000000000000000000000<identifiant fti/12345567>'

config interface 'WAN6'
  option sendopts '16:00:00:04:0e:00:05:73:61:67:65:6d 15:FSVDSL_livebox.Internet.softathome.Livebox3 11:00:00:00:00:00:00:00:00:00:00:00:<identifiant fti/1234567>'
```

# Après

/etc/config/network
```
config interface 'wan'
  option sendopts '0x4D:2b46535644534c5f6c697665626f782e496e7465726e65742e736f66746174686f6d652e4c697665626f7833 0x5a:0000000000000000000000<nouveau prefixe>:<identifiant fti/1234567>:<nouvelle suffixe>'

config interface 'WAN6'
  option sendopts '16:00:00:04:0e:00:05:73:61:67:65:6d 15:FSVDSL_livebox.Internet.softathome.Livebox3 11:00:00:00:00:00:00:00:00:00:00:00:<nouveau prefixe>:<identifiant fti/1234567>:<nouvelle suffixe>'
```

Je n'ai trouvé la signification du préfixe et suffixe et donc ne la partage pas au cas où elle soit personnelle.
