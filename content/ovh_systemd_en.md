Title: Systemd 246 on Archlinux OVH systems
Date: 2020-09-24
Category: Linux
Lang: en
Slug: systemd-246-ovh

I've been using the same [OVH Kimsufi](https://www.kimsufi.com/) server since 2013, and Systemd has changed a lot since its setup.
Systemd 246, avalaible as Archlinux packages since late July 2020, [deprecates support of the `.include` directive](https://github.com/systemd/systemd/blob/master/NEWS#L197)
This directive was used by the `netctl` service to configure network during startup:

```
% cat /etc/systemd/system/netctl@ovh_net_eth0.service
.include /usr/lib/systemd/system/netctl@.service

[Unit]
Description=A basic static ethernet connection
BindsTo=sys-subsystem-net-devices-eth0.device
After=sys-subsystem-net-devices-eth0.device
```

If the service unit was not migrated, the system will not have its network config applied during next boot:

```
Sep 24 11:25:39 ovh systemd[1]: netctl@ovh_net_eth0.service: Cannot add dependency job, ignoring: Unit netctl@ovh_net_eth0.service has a bad unit file setting.
Sep 24 11:25:39 ovh systemd[1]: netctl@ovh_net_eth0.service: Service has no ExecStart=, ExecStop=, or SuccessAction=. Refusing.
Sep 24 11:25:39 ovh systemd[1]: /etc/systemd/system/netctl@ovh_net_eth0.service:1: Assignment outside of section. Ignoring.
```

The easiest way to migrate this configuration is to disable the outdated service, remove it, and enable it again:

```
systemctl disable netctl@ovh_net_eth0.service
rm /etc/systemd/system/netctl@ovh_net_eth0.service
netctl enable ovh_net_eth0
```
